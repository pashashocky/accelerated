#include <stdexcept>
#include <algorithm>
#include <vector>
#include "Student_info.h"
#include "Grade.h"
#include "Median.h"

using std::domain_error; using std::vector;


// predicate to determine whether a student failed
bool fgrade(const Student_info& s)
{
    return s.grade() < 60;
}

// opposite to fgrade
bool pgrade(const Student_info& s)
{
    return !fgrade(s);
}

// compute student's overall grade
double grade(double midterm, double final, double homework)
{
    return 0.2 * midterm + 0.4 * final + 0.4 * homework;
}

// compute student's overall grade, without copying the hw vector<double>
double grade(double midterm, double final, const vector<double>& hw)
{
    if (hw.size() == 0)
        throw domain_error("student has done no homework");
    return grade(midterm, final, median(hw));
}
