#include "Core.h"
#include "Grad.h"
#include "Grade.h"

using std::string;
using std::istream;

string Core::name() const { return n; }

double Core::grade() const
{
    return ::grade(midterm, final, homework);
}

istream& Core::read_common(istream& in)
{
    // read and store the student's name and exam grades
    in >> n >> midterm >> final;
    return in;
}

istream& Core::read(istream& in)
{
    read_common(in);
    read_hw(in, homework);
    return in;
}

bool compare(const Core& c1, const Core& c2)
{
    return c1.name() < c2.name();
}

bool compare_Core_ptrs(const Core* cp1, const Core* cp2)
{
    return compare(*cp1, *cp2);
}

istream& Student::read(istream& is) 
{
    char ch;
    is >> ch; // get record type

    // allocate new object of appropriate type
    // use Handle<T>(T*) to build a new Handle<Core> from the pointer to that object
    // call Handle<T>::operator = to assign the Handle<Core> to the left hand side
    if (ch == 'U') {
        cp = new Core(is);
    } else {
        cp = new Grad(is);
    }
    
    return is;
}
