#include <iostream>
#include "Core.h"

using std::runtime_error;

template<class T>
Handle<T>& Handle<T>::operator=(const Handle& rhs)
{
    if (&rhs != this) {
        delete p;
        p = rhs.p ? rhs.p->clone() : 0;
    }
    return *this;
}

template <class T>
T& Handle<T>::operator*() const
{
    if (p)
        return *p;
    throw runtime_error("unbound Handle");
}

template <class T>
T* Handle<T>::operator->() const
{
    if (p)
        return p;
    throw runtime_error("unbound Handle");
}


bool compare_Core_handles(const Handle<Core>& h1, const Handle<Core>& h2)
{
    return h1->name() < h2->name();
}

template <class T>
Ref_handle<T>& Ref_handle<T>::operator=(const Ref_handle& rhs)
{
    ++*rhs.refptr;
    // free the left-hand side, destroying the pointers if appropriate
    if (--*refptr == 0) {
        delete refptr;
        delete p;
    }

    // copy values from the right hand side
    refptr = rhs.refptr;
    p  = rhs.p;
    return *this;
}

template <class T>
Ref_handle<T>::~Ref_handle()
{
    if (--*refptr == 0) {
        delete refptr;
        delete p;
    }
}
