#ifndef D_CORE_H
#define D_CORE_H 

#include <iostream>
#include <vector>

using std::runtime_error;

class Core {
    friend class Student;
    public:
        Core(): midterm(0), final(0) { };
        Core(std::istream& is) { read(is); };
        virtual ~Core() { };
        std::string name() const;
        virtual double grade() const;
        virtual std::istream& read(std::istream&);
    protected:
        virtual Core* clone() const { return new Core(*this); }
        std::istream& read_common(std::istream&);
        double midterm, final;
        std::vector<double> homework;
    private:
        std::string n;
};

template <class T> class Handle {
    public:
        Handle(): p(0) {  };
        Handle(const Handle& s): p(0) { if (s.p) p = s.p->clone(); }
        Handle& operator=(const Handle&);
        ~Handle() { delete p; }

        Handle(T* t): p(t) {  };

        operator bool() const { return p; }
        T& operator*() const;
        T* operator->() const;
    private:
        T* p;
};

class Student {
    public:
        // constructors and copy control
        Student(): cp(0) {  };
        Student(std::istream& is): cp(0) { read(is); }

        // operations
        std::istream& read(std::istream&);

        std::string name() const {
            if (cp) return cp->name();
            else throw std::runtime_error("uninitialized student");
        }
        double grade() const {
            if (cp) return cp->grade();
            else throw std::runtime_error("uninitialized student");
        }
        static bool compare(const Student& s1, const Student& s2) {
            return s1.name() < s2.name();
        }
    private:
        Handle<Core> cp;

};


template <class T> class Ref_handle {
    public:
        // manage reference ocunt as well as pointer
        Ref_handle(): refptr(new size_t(1)), p(0) {  }
        Ref_handle(T* t): refptr(new size_t(1)), p(t) {  }
        Ref_handle(const Ref_handle& h): refptr(h.refptr), p(h.p)
        {
            ++*refptr;
        }

        Ref_handle& operator=(const Ref_handle&);
        ~Ref_handle();

        operator bool() const { return p; }
        T& operator*() const
        {
            if (p)
                return *p;
            throw runtime_error("unbound Handle");
        }
        T* operator->() const
        {
            if (p)
                return p;
            throw runtime_error("unbound Handle");
        }
    private:
        T* p;
        size_t* refptr;
};

template <class T> class Ptr {
    public:
        // new member to copy the object conditionally when needed
        void make_unique() {
            if (*refptr != 1) {
                --*refptr;
                refptr = new size_t(1);
                p = p ? p->clone() : 0;
            }
        }
        // manage reference ocunt as well as pointer
        Ptr(): refptr(new size_t(1)), p(0) {  }
        Ptr(T* t): refptr(new size_t(1)), p(t) {  }
        Ptr(const Ptr& h): refptr(h.refptr), p(h.p)
        {
            ++*refptr;
        }

        Ptr& operator=(const Ptr&);
        ~Ptr();

        operator bool() const { return p; }
        T& operator*() const
        {
            if (p)
                return *p;
            throw runtime_error("unbound Handle");
        }
        T* operator->() const
        {
            if (p)
                return p;
            throw runtime_error("unbound Handle");
        }
    private:
        T* p;
        size_t* refptr;
};

template <class T>
bool compare_Core_handles(const T&, const T&);


#endif /* ifndef D_CORE_H */
