#ifndef G_MAP_H
#define G_MAP_H 

#include <vector>
#include <map>
#include <string>
#include <iostream>
#include "split.h"

typedef std::vector<std::string> Rule;
typedef std::vector<Rule> Rule_collection;
typedef std::map<std::string, Rule_collection> Grammar;

std::map<std::string, std::vector<int> > 
    xref(std::istream& in, std::vector<std::string> find_words(const std::string&) = split);
Grammar read_grammar(std::istream&);
bool bracketed(const std::string&);
int nrand(int);
void gen_aux(const Grammar&, const std::string&, std::vector<std::string>&);
std::vector<std::string> gen_sentence(const Grammar&);

#endif /* ifndef G_MAP_H */
