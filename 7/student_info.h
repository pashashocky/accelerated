#ifndef G_STUDENT_INFO_H
#define G_STUDENT_INFO_H 

#include <iostream>
#include <string>
#include <vector>
#include <list>

struct Student_info {
    std::string name;
    double midterm, final;
    std::vector<double> homework;
};

bool compare(const Student_info& x, const Student_info& y);
bool did_all_hw(const Student_info&);
std::istream& read_hw(std::istream& in, std::vector<double>& hw);
std::istream& read(std::istream& is, Student_info& s);
std::list<Student_info> extract_fails(std::list<Student_info>& students);


#endif /* ifndef G_STUDENT_INFO_H */
