#include <algorithm>
#include <ctime>
#include <iomanip>
#include <ios>
#include <iostream>
#include <stdexcept>
#include <string>
#include <cctype>
#include <list>
#include <vector>
#include <map>
#include "student_info.h"
#include "grade.h"
#include "split.h"
#include "analysis.h"
#include "map.h"

using std::cout; using std::endl;
using std::map; using std::cin;
using std::string; using std::vector;

int main()
{
    // // call xref using split by default
    // map<string, vector<int> > ret = xref(cin);
    //
    // // write the results
    // for (map<string, vector<int> >::const_iterator it = ret.begin(); it != ret.end(); ++it) {
    //     // write the word
    //     cout << it->first << " occurs on line(s): ";
    //
    //     // followed by one or more line numbers
    //     vector<int>::const_iterator line_it = it->second.begin();
    //     cout << *line_it; // write the first line number
    //
    //     ++line_it;
    //     // write the rest of the line numbers, if any
    //     while (line_it != it->second.end()) {
    //         cout << ", " << *line_it;
    //         ++line_it;
    //     }
    //     // write a new line
    //     cout << endl;
    // }

    Grammar g = read_grammar(cin);
    int i = 0;
    while (i < 10) {
        // generate the sentence
        vector<string> sentence = gen_sentence(g);

        // write the first word, if any
        vector<string>::const_iterator it = sentence.begin();
        if (!sentence.empty()) {
            cout << *it;
            ++it;
        }

        // write the rest of the words, each preceded by a space
        while (it != sentence.end()) {
            cout << " " << *it;
            ++it;
        }
        cout << endl;
        ++i;
    }

    return 0;
}
