#include <algorithm>
#include <iomanip>
#include <ios>
#include <iostream>
#include <stdexcept>
#include <string>
#include <cctype>
#include <list>
#include "student_info.h"
#include "grade.h"
#include "split.h"
#include "analysis.h"

using std::cout; using std::endl;
using std::vector; using std::cin;

int main()
{
    vector<Student_info> did, didnt;
    Student_info student;

    // read all the records, separating them based on student doing all hw
    while (read(cin, student)) {
        if (did_all_hw(student))
            did.push_back(student);
        else
            didnt.push_back(student);
    }

    // check that both groups contain data
    if (did.empty()) {
        cout << "No student has done all the homework!" << endl;
        return 1;
    }
    if (didnt.empty()) {
        cout << "All students did all the homework!" << endl;
        return 1;
    }

    // do the analyses
    write_analysis(cout, "median", median_analysis, did, didnt);
    write_analysis(cout, "average", average_analysis, did, didnt);
    write_analysis(cout, "median of homework turned in", 
            optimistic_median_analysis, did, didnt);

    return 0;
}
