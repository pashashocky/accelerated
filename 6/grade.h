#ifndef G_GRADE_H
#define G_GRADE_H 

#include <vector>
#include "student_info.h"

bool fgrade(const Student_info&);
bool pgrade(const Student_info&);
double grade(const Student_info&);
double grade(double, double, const std::vector<double>&);
double grade(double, double, double);
double average_grade(const Student_info&);
double grade_aux(const Student_info&);
double optimistic_median(const Student_info&);

#endif /* ifndef G_GRADE_H */
