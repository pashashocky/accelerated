#include <stdexcept>
#include <algorithm>
#include <vector>
#include "student_info.h"
#include "grade.h"
#include "median.h"

using std::domain_error; using std::vector;


// predicate to determine whether a student failed
bool fgrade(const Student_info& s)
{
    return grade(s) < 60;
}

// opposite to fgrade
bool pgrade(const Student_info& s)
{
    return !fgrade(s);
}

// compute student's overall grade for a Student_info struct
double grade(const Student_info& s)
{
    return grade(s.midterm, s.final, s.homework);
}

// compute student's overall grade
double grade(double midterm, double final, double homework)
{
    return 0.2 * midterm + 0.4 * final + 0.4 * homework;
}

// compute student's overall grade, without copying the hw vector<double>
double grade(double midterm, double final, const vector<double>& hw)
{
    if (hw.size() == 0)
        throw domain_error("student has done no homework");
    return grade(midterm, final, median(hw));
}

// returns the students grade calculated with the average of the homework
double average_grade(const Student_info& s)
{
    return grade(s.midterm, s.final, average(s.homework));
}

// function that handles exceptions of grade when no homework has been done
// non overloaded.
double grade_aux(const Student_info& s)
{
    try {
        return grade(s);
    } catch (domain_error) {
        return grade(s.midterm, s.final, 0);
    }
}

// median of the nonzero elements of s.homework, or 0 if its empty
double optimistic_median(const Student_info& s)
{
    vector<double> nonzero;
    remove_copy(s.homework.begin(), s.homework.end(), back_inserter(nonzero), 0);

    if (nonzero.empty())
        return grade(s.midterm, s.final, 0);
    else
        return grade(s.midterm, s.final, median(nonzero));
}
