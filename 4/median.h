#ifndef G_MEDIAN_H
#define G_MEDIAN_H 

#include <vector>    // declaration of vector

double median(std::vector<double>);

#endif /* ifndef G_MEDIAN_H */
