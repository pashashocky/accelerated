#include <stdexcept>
#include <vector>
#include "student_info.h"
#include "grade.h"
#include "median.h"

using std::domain_error; using std::vector;


// compute student's overall grade for a Student_info struct
double grade(const Student_info& s)
{
    return grade(s.midterm, s.final, s.homework);
}

// compute student's overall grade
double grade(double midterm, double final, double homework)
{
    return 0.2 * midterm + 0.4 * final + 0.4 * homework;
}

// compute student's overall grade, without copying the hw vector<double>
double grade(double midterm, double final, const vector<double>& hw)
{
    if (hw.size() == 0)
        throw domain_error("student has done no homework");
    return grade(midterm, final, median(hw));
}
