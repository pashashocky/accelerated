#include "Grade.h"
#include "Student_info.h"
#include <algorithm>
#include <vector>

using std::istream; using std::vector; using std::cout; using std::endl;
using std::vector;

// class constructors
Student_info::Student_info(): midterm(0), final(0) {}
Student_info::Student_info(istream& is) { read(is); }

// read student info as a member of the struct
istream& Student_info::read(istream& is)
{
    is >> n >> midterm >> final;
    read_hw(is, homework);
    return is;
}

// grade member
double Student_info::grade() const
{
    return ::grade(midterm, final, homework);
}

// compare Student_info structures by name for sorting
bool compare(const Student_info& x, const Student_info& y)
{
    return x.name() < y.name();
}

// read homework grades from an input stream (cin) into a vector<double>
istream& read_hw(istream& in, vector<double>& hw)
{
    if (in) {
        // get rid of previous contents
        hw.clear();

        // read homework grades
        double x;
        cout << "Enter the homework grades for this student, finish with end-of-file." << endl;
        while (in >> x)
            hw.push_back(x);

        // clear the stream so that input will work for the next student
        in.clear();
    }

    return in;
}
