#include "Core.h"
#include "Grad.h"
#include "Grade.h"

using std::string;
using std::istream;

string Core::name() const { return n; }

double Core::grade() const
{
    return ::grade(midterm, final, homework);
}

istream& Core::read_common(istream& in)
{
    // read and store the student's name and exam grades
    in >> n >> midterm >> final;
    return in;
}

istream& Core::read(istream& in)
{
    read_common(in);
    read_hw(in, homework);
    return in;
}

bool compare(const Core& c1, const Core& c2)
{
    return c1.name() < c2.name();
}

bool compare_Core_ptrs(const Core* cp1, const Core* cp2)
{
    return compare(*cp1, *cp2);
}

Student::Student(const Student& s): cp(0)
{
    if (s.cp) cp = s.cp->clone();
}

Student& Student::operator=(const Student& s)
{
    if (&s != this) {
        delete cp;
        if (s.cp)
            cp = s.cp->clone();
        else
            cp = 0;
    }
    return *this;
}

istream& Student::read(istream& is) 
{
    delete cp; // clear pointer if any
    
    char ch;
    is >> ch; // get record type

    if (ch == 'U') {
        cp = new Core(is);
    } else {
        cp = new Grad(is);
    }
    
    return is;
}
