#include <algorithm>
#include "Grad.h"
#include "Student_info.h"

using std::min;
using std::istream;

istream& Grad::read(istream& in)
{
    read_common(in);
    in >> thesis;
    read_hw(in, homework);
    return in;
}

double Grad::grade() const
{
    return min(Core::grade(), thesis);
}
