#ifndef D_CORE_H
#define D_CORE_H 

#include <iostream>
#include <vector>

class Core {
    friend class Student;
    public:
        Core(): midterm(0), final(0) { };
        Core(std::istream& is) { read(is); };
        virtual ~Core() { };
        std::string name() const;
        virtual double grade() const;
        virtual std::istream& read(std::istream&);
    protected:
        virtual Core* clone() const { return new Core(*this); }
        std::istream& read_common(std::istream&);
        double midterm, final;
        std::vector<double> homework;
    private:
        std::string n;
};

class Student {
    public:
        // constructors and copy control
        Student(): cp(0) {  };
        Student(std::istream& is): cp(0) { read(is); }
        Student(const Student&);
        Student& operator=(const Student&);
        ~Student() { delete cp; };

        // operations
        std::istream& read(std::istream&);

        std::string name() const {
            if (cp) return cp->name();
            else throw std::runtime_error("uninitialized student");
        }
        double grade() const {
            if (cp) return cp->grade();
            else throw std::runtime_error("uninitialized student");
        }
        static bool compare(const Student& s1, const Student& s2) {
            return s1.name() < s2.name();
        }
    private:
        Core* cp;

};

#endif /* ifndef D_CORE_H */
