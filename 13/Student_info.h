#ifndef G_STUDENT_INFO_H
#define G_STUDENT_INFO_H 

#include <iostream>
#include <string>
#include <vector>
#include <list>

class Student_info {
    public:
        Student_info();
        Student_info(std::istream&);
        double grade() const;
        std::istream& read(std::istream&);
        std::string name() const { return n; }
        bool valid() const { return !homework.empty(); }

    private:
        std::string n;
        double midterm, final;
        std::vector<double> homework;
};

bool compare(const Student_info& x, const Student_info& y);
std::istream& read_hw(std::istream&, std::vector<double>&);

#endif /* ifndef G_STUDENT_INFO_H */
