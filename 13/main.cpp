#include <vector>
#include <iomanip>
#include <ios>
#include <iostream>
#include <algorithm>
#include "Core.h"
#include "Grad.h"

using namespace std;

int main()
{
    vector<Student> students; // store pointers not objects
    Student record;           // temporary is a pointer as well
    string::size_type maxlen = 0;

    // read and store the data
    while (record.read(cin)) { 
        maxlen = max(maxlen, record.name().size());
        students.push_back(record);
    }

    // alphabetize the student records
    sort(students.begin(), students.end(), Student::compare);

    // write the names and grades
    for (vector<Student>::size_type i = 0; i < students.size(); ++i) {
        cout << students[i].name() << string(maxlen + 1 - students[i].name().size(), ' ');

        try {
            double final_grade = students[i].grade(); // Core or Grad pointer by auto.
            streamsize prec = cout.precision();
            cout << setprecision(3) << final_grade << setprecision(prec) << endl;
        } catch (domain_error e) {
            cout << e.what() << endl;
        }
    }

    return 0;
}
