#include "student_info.h"
#include <list>

using std::istream; using std::vector; using std::cout; using std::endl;

// compare Student_info structures by name for sorting
bool compare(const Student_info& x, const Student_info& y)
{
    return x.name < y.name;
}

// read student info into its struct object
istream& read(istream& is, Student_info& s)
{
    // read and store students name, midterm and final grades.
    cout << "Input student name, midterm and final grades." << endl;
    is >> s.name >> s.midterm >> s.final;

    read_hw(is, s.homework); // read and store the homework grades
    return is;
}

// read homework grades from an input stream (cin) into a vector<double>
istream& read_hw(istream& in, vector<double>& hw)
{
    if (in) {
        // get rid of previous contents
        hw.clear();

        // read homework grades
        double x;
        cout << "Enter the homework grades for this student, finish with end-of-file." << endl;
        while (in >> x)
            hw.push_back(x);

        // clear the stream so that input will work for the next student
        in.clear();
    }

    return in;
}

// extracts the failed students into a new 'fail' list from a list of all students
std::list<Student_info> extract_fails(std::list<Student_info>& students)
{
    std::list<Student_info> fail;
    std::list<Student_info>::iterator iter = students.begin();

    while (iter != students.end()) {
        if (fgrade(*iter)) {
            fail.push_back(*iter);
            iter = students.erase(iter);
        } else 
            ++iter;
    }

    return fail;
}

