#include <algorithm>
#include <iomanip>
#include <ios>
#include <iostream>
#include <stdexcept>
#include <string>
#include <cctype>
#include <list>
#include "student_info.h"
#include "grade.h"
#include "split.h"

using std::cin; using std::cout;
using std::endl; using std::vector;
using std::streamsize; using std::setprecision; 
using std::list; using std::domain_error;
using std::string; using std::max;

int main()
{
    string s;
    // read and split each line of input
    while (getline(cin, s)) {
        vector<string> v = split(s);

        v = frame(v);
        v = vcat(v, v);
        v = hcat(v, v);
        // write each word in v
        for (vector<string>::size_type i = 0; i != v.size(); ++i) {
            cout << v[i] << endl;            
        }
    }

    return 0;
}
