#ifndef G_PTR_H
#define G_PTR_H 

#include <iostream>

template <class T> class Ptr {
    public:
        // new member to copy the object conditionally when needed
        void make_unique() {
            if (*refptr != 1) {
                --*refptr;
                refptr = new size_t(1);
                p = p ? p->clone() : 0;
            }
        }
        // manage reference ocunt as well as pointer
        Ptr(): refptr(new size_t(1)), p(0) {  }
        Ptr(T* t): refptr(new size_t(1)), p(t) {  }
        Ptr(const Ptr& h): refptr(h.refptr), p(h.p)
        {
            ++*refptr;
        }

        Ptr& operator=(const Ptr&);
        ~Ptr() {}

        operator bool() const { return p; }
        T& operator*() const
        {
            if (p)
                return *p;
            throw std::runtime_error("unbound Handle");
        }
        T* operator->() const
        {
            if (p)
                return p;
            throw std::runtime_error("unbound Handle");
        }
    private:
        T* p;
        size_t* refptr;
};


#endif /* ifndef G_PTR_H */
